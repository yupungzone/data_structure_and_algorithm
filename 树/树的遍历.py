"""
前序遍历：先访问根节点，再递归地前序访问左子树、最后前序访问右子树；
中序遍历：左子树——》根节点——》右子树；
后序遍历：左子树——》右子树——》根节点
"""

# 前序遍历
def preorder(tree):
    if tree:
        print(tree.getRootVal())
        preorder(tree.getLeftChild())
        preorder(tree.getRightChild())



# 后序遍历
def postorder(tree):
    if tree != None:
        postorder(tree.getLeftChild())
        postorder(tree.getRightChild())
        print(tree.getRootVal())



# 中序遍历
def inorder(tree):
    if tree != None:
        inorder(tree.getLeftChild())
        print(tree.getRootVal())
        inorder(tree.getRightChild())
