# 表达式解析

from pythonds.basic.stack import Stack
from pythonds.trees.binaryTree import BinaryTree


def buildParseTree(fpexp):
    fplist = fpexp.split()
    pStack = Stack()
    eTree = BinaryTree('')
    pStack.push(eTree)    # 入栈下降
    currentTree = eTree
    for i in fplist:
        # 表达式开始
        if i == '(':
            currentTree.insertLeft('')
            pStack.push(currentTree)    # 入栈下降
            currentTree = currentTree.getLeftChild()
        # 操作数
        elif i not in ['+', '-', '*', '/', ')']:
            currentTree.setRootVal(int(i))
            parent = pStack.pop()   # 出栈上升
            currentTree = parent
        # 操作符
        elif i in ['+', '-', '*', '/']:
            currentTree.setRootVal(i)
            currentTree.insertRight('')
            pStack.push(currentTree)
            currentTree = currentTree.getRightChild()
        # 表达式结束
        elif i == ')':
            currentTree = pStack.pop()    # 出栈上升
        else:
            raise ValueError
    return eTree


"""
# 减少运算符的判断次数，减少if语句的个数，增加程序的可读性
import operator
op = operator.add
"""

import operator


def evaluate(parseTree):
    opers = {
        "+": operator.add, '-': operator.sub,
        '*': operator.mul, '/': operator.truediv
    }

    # 缩小规模
    leftC = parseTree.getLeftChild()
    rightC = parseTree.getRightChild()

    if leftC and rightC:
        fn = opers[parseTree.getRootVal()]
        return fn(evaluate(leftC), evaluate(rightC))   # 递归调用
    else:
        return parseTree.getRootVal()   # 基本结束条件