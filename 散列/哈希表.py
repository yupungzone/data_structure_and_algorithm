# python自带的散列函数库hashlib
import hashlib

pass1 = hashlib.md5("hello world!".encode('utf-8')).hexdigest()
pass2 = hashlib.sha1("hello world!".encode('utf-8')).hexdigest()
print(pass1, "\n", pass2)