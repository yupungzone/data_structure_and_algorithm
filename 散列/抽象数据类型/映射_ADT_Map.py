# 字典
# key：value
# 结构关键：键-值关联的无序集合
class HashTable:
    def __init__(self):
        self.size = 11   # 设定散列表的大小，一般选择为素数
        self.slots = [None] * self.size
        self.data = [None] * self.size

    def hashfunction(self, key):
        return key % self.size

    def rehash(self, oldhash):
        return (oldhash + 1) % self.size

    def put(self, key, data):
        hashvalue = self.hashfunction(key)

        # key不存在，未冲突
        if self.slots[hashvalue] == None:
            self.slots[hashvalue] = key
            self.data[hashvalue] = data
        else:
            # key已存在，替换val
            if self.slots[hashvalue] == key:
                self.data[hashvalue] = data   # replace
            else:
                nextslot = self.rehash(hashvalue)
                # 散列冲突，再散列，直到找到空槽或者key
                while self.slots[nextslot] != None and self.slots[nextslot] != key:
                    nextslot = self.rehash(nextslot)

                if self.slots[nextslot] == None:
                    self.slots[nextslot] = key
                    self.data[nextslot] = data
                else:
                    self.data[nextslot] = data  # replace

    def get(self, key):
        # 标记散列值为查找起点
        startslot = self.hashfunction(key)

        data = None
        stop = False
        found = False
        position = startslot

        # 找key，直到空槽或回到起点
        while self.slots[position] != None and not found and not stop:
            if self.slots[position] == key:
                found = True
                data = self.data[position]
            else:
                # 未找到key，再散列继续找
                position = self.rehash(position)
                if position == startslot:
                    stop = True   # 回到起点，停止
        return data

    """以下：通过特殊方法实现[]访问"""
    def __getitem__(self, key):
        return self.get(key)

    def __setitem__(self, key, data):
        self.put(key, data)