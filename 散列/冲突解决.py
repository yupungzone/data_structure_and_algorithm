"""
方案一： 开放定址技术
# 再散列 rehashing
newhashvalue = rehash(oldhashvalue)
# 对于线性探测
rehash(pos) = (pos + 1) % sizeoftable

# +3 跳跃式探测
rehash(pos) = (pos + 3) % sizeoftable

# 跳跃式探测的再散列通式
rehash(pos) = (pos + skip) % sizeoftable
技巧：将散列表的大小设为素数


方案二： 数据项链Chaining
将容纳单个数据项的槽扩展为容纳数据项集合（或者对数据项链表的引用）
"""