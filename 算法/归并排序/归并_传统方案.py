def mergeSort(alist):
    if len(alist) > 1:   # 基本结束条件
        mid = len(alist) // 2
        lefthalf = alist[:mid]
        righthalf = alist[mid:]

        # 递归调用
        mergeSort(lefthalf)
        mergeSort(righthalf)

        i = j = k = 0
        # 拉链式交错把左右半部从小到大归并到结果列表中
        while i < len(lefthalf) and j < len(righthalf):
            if lefthalf[i] < righthalf[j]:
                alist[k] = lefthalf[i]
                i += 1
            else:
                alist[k] = righthalf[j]
                j += 1
            k += 1

        # 归并左半部剩余项
        while i < len(lefthalf):
            alist[k] = lefthalf[i]
            i += 1
            k += 1

        # 归并右半部剩余项
        while j < len(lefthalf):
            alist[k] = lefthalf[j]
            j += 1
            k += 1