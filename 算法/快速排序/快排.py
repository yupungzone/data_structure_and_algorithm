"""
快排思想：
依据”中值“数据项来把数据表分为 两半：
小于中值的一半 和 大于中值的一半，
然后每部分分别进行快速排序（递归）。
对于快排，最重要的是 中值的选取。
时间复杂度： O(n*log n)
"""

def quickSort(alist):
    quickSortHelper(alist, 0, len(alist)-1)

def quickSortHelper(alist, first, last):
    # 基本结束条件
    if first < last:

        # 分裂
        splitpoint = partition(alist, first, last)

        # 递归调用
        quickSortHelper(alist, first, splitpoint-1)
        quickSortHelper(alist, splitpoint+1, last)

def partition(alist, first, last):
    pivotvalue = alist[first]    # 选定中值
    # 左右标初值
    leftmark = first + 1
    rightmark = last

    done = False
    while not done:

        # 左标右移
        while leftmark <= rightmark and alist[leftmark] <= pivotvalue:
            leftmark += 1

        # 右标左移
        while alist[rightmark] >= pivotvalue and rightmark >= leftmark:
            rightmark -= 1

        # 两标相错就结束移动
        if rightmark < leftmark:
            done = True
        else:
            # 左右标的值交换
            alist[leftmark], alist[rightmark] = alist[rightmark], alist[leftmark]

    # 中值就位
    alist[first], alist[rightmark] = alist[rightmark], alist[first]

    # 中值点，也就是分裂点
    return rightmark


alist = [53, 89, 213, 23, 90, 12, 45]
quickSort(alist)
print(alist)