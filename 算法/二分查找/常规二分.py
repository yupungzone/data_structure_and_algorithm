# _*_ coding utf-8 _*_
# editor : wutongyu
# TIME : 2021/6/2015:21
# file_name :常规二分.py
# TOOL : PyCharm

def binarySearch(alist, item):
    first = 0
    last = len(alist) - 1
    found = False

    while first <= last and not found:
        midpoint = (first + last) // 2
        # 中间项比对
        if alist[midpoint] == item:
            found = True
        else:
            # 缩小比对范围
            if item < alist[midpoint]:
                last = midpoint - 1
            else:
                first = midpoint + 1

    return found


testlist = [0,1,2,3,4,5,6]
print(binarySearch(testlist, 3))
print(binarySearch(testlist, 13))