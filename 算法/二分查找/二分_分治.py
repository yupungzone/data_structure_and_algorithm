# _*_ coding utf-8 _*_
# editor : wutongyu
# TIME : 2021/6/2015:22
# file_name :二分_分治.py
# TOOL : PyCharm

"""
二分查找时间复杂度：O（log n)
切片时间复杂度：O(k)
"""


def binarySearch(alist, item):
    if len(alist) == 0:
        return False   # 结束条件
    else:
        midpoint = len(alist) // 2
        if alist[midpoint] == item:
            return True
        else:
            # 缩小规模
            if item < alist[midpoint]:
                return binarySearch(alist[:midpoint], item)  # 调用自身
            else:
                return binarySearch(alist[midpoint + 1:], item)