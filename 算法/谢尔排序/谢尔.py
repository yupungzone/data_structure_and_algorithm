"""基于插入排序，对列表进行子列表排序，加快排序速度，优化性能"""
def shellSort(alist):
    sublistcount = len(alist) // 2   # 间隔设定
    while sublistcount > 0:
        for startposition in range(sublistcount):   # 子列表排序
            gapInsertionSort(alist, startposition, sublistcount)

        print("After increments of size", sublistcount, "The list is", alist)

        sublistcount = sublistcount // 2  # 间隔缩小


def gapInsertionSort(alist, start, gap):
    for i in range(start+gap, len(alist), gap):
        currentvalue = alist[i]
        position = i

        while position >= gap and alist[position-gap] > currentvalue:
            alist[position] = alist[position-gap]
            position = position - gap

        alist[position] = currentvalue
