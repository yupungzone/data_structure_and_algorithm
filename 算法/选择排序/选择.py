"""
每趟仅进行一次交换，记录最大项的位置，最后再跟本趟最后一项交换
时间复杂度：O(n*n)
交换次数：O(n)
"""

def selectionSort(alist):
    for fillslot in range(len(alist)-1, 0, -1):
        positionOfMax = 0
        for location in range(1, fillslot+1):
            if alist[location] > alist[positionOfMax]:
                positionOfMax = location

        alist[fillslot], alist[positionOfMax] = alist[positionOfMax], alist[fillslot]