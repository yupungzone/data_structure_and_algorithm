# _*_ coding utf-8 _*_
# editor : wutongyu
# TIME : 2021/6/2016:10
# file_name :冒泡_优化.py
# TOOL : PyCharm


def shortBubbleSort(alist):
    exchanges = True
    passnum = len(alist) - 1
    while passnum > 0 and exchanges:
        exchanges = False
        for i in range(passnum):
            if alist[i] > alist[i+1]:
                exchanges = True
                alist[i], alist[i+1] = alist[i+1], alist[i]
        passnum -= 1