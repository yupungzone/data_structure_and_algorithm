# _*_ coding utf-8 _*_
# editor : wutongyu
# TIME : 2021/6/2016:02
# file_name :冒泡.py
# TOOL : PyCharm
"""时间复杂度： O(n*n)"""

def bubbleSort(alist):
    for passnum in range(len(alist)-1, 0, -1):
        for i in range(passnum):   # n-1趟
            if alist[i] > alist[i+1]:
                temp = alist[i]
                alist[i] = alist[i+1]
                alist[i+1] = temp   # alist[i], alist[i+1] = alist[i+1], alist[i]  序错，交换


alist = [54, 23, 787, 21, 78, 96]
bubbleSort(alist)
print(alist)