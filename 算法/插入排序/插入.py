def insertSort(alist):
    for index in range(1, len(alist)):

        currentvalue = alist[index]     # 新项/插入项
        position = index

        while position > 0 and alist[position-1] > currentvalue:
            alist[position] = alist[position-1]
            position = position - 1   # 比对、移动

        alist[position] = currentvalue   # 插入新项


# 由于移动操作仅包含1次赋值，是交换操作的1/3，所以插入排序性能会较好一些。