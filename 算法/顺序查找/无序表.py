# _*_ coding utf-8 _*_
# editor : wutongyu
# TIME : 2021/6/2015:35
# file_name :无序表.py
# TOOL : PyCharm


def sequentialSearch(alist, item):
    pos = 0
    found = False

    while pos < len(alist) and not found:
        if alist[pos] == item:
            found = True
        else:
            pos += 1

    return found