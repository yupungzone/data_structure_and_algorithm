# _*_ coding utf-8 _*_
# editor : wutongyu
# TIME : 2021/6/2015:37
# file_name :有序表.py
# TOOL : PyCharm


def orderdSequentialSearch(alist, item):
    pos = 0
    found = False
    stop = False

    while pos < len(alist) and not found and not stop:
        if alist[pos] == item:
            found = True
        else:
            if alist[pos] > item:
                stop = True
            else:
                pos += 1

    return found